const chai = require('chai');
const assert = require('chai').assert;

const Tabula = require('../index.js');

const simpleTable = {
    "treasure": {
        "unguarded": [
            "1 gold coin",
            "sword",
            "a bag of 10 coins",
            "minor magical trinket",
        ],
        "coins": [
            "[2d10*10] gold coins"
        ],
        "Gems": [
            "a gem worth [3d6*100] coins"
        ]
    }
};

const random = {
  real() { return 0.49 },
  pick(coll) { return coll[0]; }
};

describe('Tabula', function () {
    const tables = new Tabula(simpleTable, random);
    it("should return a result", function () {
        assert.equal(
            tables.roll("treasure", "unguarded"),
            "1 gold coin"
        );
    });

    it("should roll dice", function () {
        assert.equal(
            tables.roll("treasure", "coins"),
            "100 gold coins"
        );
    });

    it ("should match groups and tables case-insensitively", function () {
        assert.equal(
            tables.roll("Treasure", "gems"),
            "a gem worth 900 coins"
        )
    })
});
