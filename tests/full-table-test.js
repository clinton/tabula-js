const chai = require('chai');
const assert = require('chai').assert;
const sinon = require('sinon');

const Tabula = require('../index.js');
const tables = require('../data/perilous-wilds.js');
const Random = require('./programmable-random.js')

describe('Tabula', function () {
    describe('local jumps', function () {
        it('should jump correctly', function () {
            const tabula = new Tabula(tables, new Random({pick: 0}));
            const output = tabula.roll("steading", "steading");
            assert.equal(
                output,
                "Village; Natural defenses; Surrounded by arid uncultivable land"
            )
        })
    });

    describe("group jumps", function () {
        it('should jump correctly', function () {
            var random = new Random({pick: 1});
            const tabula = new Tabula(tables, random);
            const output = tabula.roll("creature", "creature");
            assert.equal(
                output,
                "snail/slug/worm; activity: fighting/at war; disposition: hostile/aggressive; no. appearing: Solitary (1)"
            )
        })
    })

    describe("group and table names", function () {
        it("can have a slash", function () {
            const tabula = new Tabula(tables, new Random({pick: 3}));
            // will have remains/debris in result
            const output = tabula.roll("discovery", "evidence");
            assert.equal(
                output,
                "remains/debris: bones; age: young/recent; visibility: partly covered/overgrown/hidden"
            );
        })
    })
})
