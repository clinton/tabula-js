"use strict";

const R = require("ramda");

class Random {
    constructor(opts) {
        opts = opts || {}
        this.defaultReal = opts.real || 0.5;
        this.defaultPick = opts.pick || 0;
        this.reals = [];
        this.picks = [];
    }

    real() {
        if (this.reals.length > 0) {
            return this.reals.shift();
        } else {
            return this.defaultReal;
        }
    }

    pick(collection) {
        var idx;
        if (this.picks.length > 0) {
            idx = this.picks.shift()
        } else {
            idx = this.defaultPick;
        }

        idx = idx % collection.length;
        return collection[idx];
    }

    copy() {
        return R.clone(this);
    }
}

module.exports = Random;
