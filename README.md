# Tabula.js

Tabula is a library for making random selections from linked tables, useful
mainly for role-playing games. An example of this table type is
[this random NPC generator for _Swords & Wizardry_](http://www.swordsandwizardry.com/grimnpcgenerator.pdf).

It is inspired by, but shares no code with, the [tabula](https://github.com/Asmor/tabula) Python script. Instead of using text files, tabula.js uses JavaScript objects as its tables.

## Example

```js
var Tabula = require("tabula")
var perilousTables = require("tabula/data/perilous-wilds")
var tables = new Tabula(perilousTables)
tables.roll("creature", "beast")
// 'locust/dragonfly/moth'
tables.roll("place", "place")
// 'The Stoney Souls'
tables.roll("place", "place")
// 'The Hill of (the) Fire'
```

# License

This code is licensed under the MIT License, as shown in [LICENSE.txt](LICENSE.txt), with
the exception of all data files, which contain their license data within them.

The data in `data/perilous-wilds.js` is a derivative of [Perilous Wilds](http://www.drivethrurpg.com/product/156979/The-Perilous-Wilds) by Jason
Lutes, used under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
