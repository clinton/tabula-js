const Roll = require("roll");
const Random = require("random-js");

function lowerCaseKeys(obj) {
    Object.keys(obj).forEach(function(key) {
        var k = key.toLowerCase();
        var v = obj[key];

        if (typeof v == 'object') {
            v = lowerCaseKeys(v);
        }

        if (k != key) {
            obj[k] = v;
            delete obj[key];
        } else {
            obj[key] = v;
        }
    });

    return obj;
}

var Tabula = function Tabula(tables, random) {
    this.tables = lowerCaseKeys(tables);
    random = random || new Random();
    this.random = random;
    this.dice = new Roll(function () { return random.real(0, 1); });
    this.scanner = new RegExp("\\[([^\\]]+?)\\]", "g")
};

Tabula.prototype.roll = function roll(group, table) {
    group = group.toLowerCase();
    table = table.toLowerCase();
    var choice = this.random.pick(this.tables[group][table]);
    choice = this.parseChoice(group, choice);
    return choice;
};

Tabula.prototype.parseChoice = function parseChoice(group, choice) {
    // scan string for bracketed text
    // for each bracket
    // if -> {table}, then replace with this.roll(group, table)
    // if {aGroup} -> {table}, then replace with this.roll(aGroup, table)
    // else replace with this.dice(text).result
    var dice = this.dice;
    var table = this;

    var replacer = function (match, p1) {
        var m;
        if (m = p1.match(/^\s*(\w.*?\w)\s*->\s*(\w.*\w)\s*$/)) {
            return table.roll(m[1], m[2]);
        } else if (m = p1.match(/^\s*->\s*(\w.*\w)\s*$/)) {
            return table.roll(group, m[1]);
        } else {
            return dice.roll(p1).result;
        }
    }

    var output = choice.replace(this.scanner, replacer);
    return output;
};

module.exports = Tabula;
